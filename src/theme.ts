import brown from '@mui/material/colors/brown';
import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    primary: {
      ...brown,
      main: '#2d2417',
    },
  },
});

export default theme;
