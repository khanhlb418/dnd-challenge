import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { useTranslation } from 'react-i18next';

import EmptyPlaceholder from '@/modules/core/components/EmptyPlaceholder';
import DragonEye from '@/modules/core/icons/DragonEye';
import { useFavoriteSpells } from '@/modules/favorite/hooks/useFavoriteSpells';
import SpellList from '@/modules/spell/components/SpellList';

const FavoritePage = () => {
  const { t } = useTranslation();
  const { favoriteSpells, isFavoriteSpell, removeFavoriteSpell } = useFavoriteSpells();

  return (
    <Box sx={{ padding: 1.5 }}>
      <Grid container gap={1} alignItems="center" justifyContent="center">
        <Grid item>
          <DragonEye sx={{ width: 56, height: 56 }} />
        </Grid>
        <Grid item>
          <Typography variant="h4" sx={{ fontFamily: "'Henny Penny', monospace" }}>
            {t('FAVORITE_SPELLS')}
          </Typography>
        </Grid>
      </Grid>
      {favoriteSpells.length ? (
        <SpellList list={favoriteSpells} isFavoriteSpell={isFavoriteSpell} onRemoveFavorite={removeFavoriteSpell} />
      ) : (
        <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexGrow: 1, mt: 8 }}>
          <EmptyPlaceholder />
        </Box>
      )}
    </Box>
  );
};

export default FavoritePage;
