import { FAVORITE_SPELLS_DB_KEY } from '@/config';
import { getFromDB, saveToDB } from '@/modules/core/persistence/db';
import { Spell } from '@/modules/spell/types';

export const saveFavoriteSpellsToDB = (favoriteSpells: Spell[]) => {
  return saveToDB(FAVORITE_SPELLS_DB_KEY, JSON.stringify(favoriteSpells));
};

export const getFavoriteSpellsFromDB = async () => {
  const rawFavoriteSpells = await getFromDB(FAVORITE_SPELLS_DB_KEY);

  try {
    const parsedFavoriteSpells = JSON.parse(rawFavoriteSpells);

    if (Array.isArray(parsedFavoriteSpells) && parsedFavoriteSpells.length > 0) return parsedFavoriteSpells as Spell[];

    return [];
  } catch (error) {
    console.error(error);

    return [];
  }
};
