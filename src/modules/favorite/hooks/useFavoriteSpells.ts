import { useEffect, useState } from 'react';

import { FavoriteSpellService } from '@/modules/favorite/services/favoriteSpell';
import { Spell } from '@/modules/spell/types';

export const useFavoriteSpells = () => {
  const [favoriteSpells, setFavoriteSpells] = useState<Spell[]>([]);

  useEffect(() => {
    const getLocalFavoriteSpells = async () => {
      const favoriteSpells = await FavoriteSpellService.getFavoriteSpells();

      setFavoriteSpells(favoriteSpells.sort((spellA, spellB) => spellA.name.localeCompare(spellB.name)));
    };

    getLocalFavoriteSpells();
  }, []);

  const addFavoriteSpell = async (spell: Spell) => {
    await FavoriteSpellService.addFavoriteSpell(spell);

    setFavoriteSpells((prevSpells) => [...prevSpells, spell]);
  };

  const removeFavoriteSpell = async (spell: Spell) => {
    await FavoriteSpellService.removeFavoriteSpell(spell);

    setFavoriteSpells((prevSpells) => prevSpells.filter((prevSpell) => prevSpell.index !== spell.index));
  };

  const isFavoriteSpell = (spell: Spell) => {
    return favoriteSpells.some((favoriteSpell) => favoriteSpell.index === spell.index);
  };

  return {
    favoriteSpells,
    addFavoriteSpell,
    removeFavoriteSpell,
    isFavoriteSpell,
  };
};
