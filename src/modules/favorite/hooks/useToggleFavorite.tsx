import FavoriteIcon from '@mui/icons-material/Favorite';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import IconButton from '@mui/material/IconButton';

import { Spell } from '@/modules/spell/types';

export interface UseToggleFavoriteProps {
  spell: Spell;
  isFavorite?: boolean;
  onAddFavorite?: (spell: Spell) => void;
  onRemoveFavorite?: (spell: Spell) => void;
}

const useToggleFavorite = ({ spell, isFavorite, onAddFavorite, onRemoveFavorite }: UseToggleFavoriteProps) => {
  const toggleFavorite = (event: React.MouseEvent) => {
    event.stopPropagation();

    if (isFavorite) {
      if (onRemoveFavorite) onRemoveFavorite(spell);

      return;
    }

    if (onAddFavorite) {
      return onAddFavorite(spell);
    }
  };

  const toggleFavoriteButton = (
    <IconButton color="inherit" onClick={toggleFavorite} data-testid={`spell-item-favorite-button-${spell.index}`}>
      {isFavorite ? <FavoriteIcon color="error" /> : <FavoriteBorderIcon />}
    </IconButton>
  );

  return { toggleFavorite, toggleFavoriteButton };
};

export default useToggleFavorite;
