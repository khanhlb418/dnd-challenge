import { useEffect, useRef } from 'react';

export const usePrevious = <T>(value: T, omitFalsy = false) => {
  const ref = useRef<T>();

  useEffect(() => {
    if (omitFalsy && !value) return;

    ref.current = value;
  }, [value, omitFalsy]);

  return ref.current;
};
