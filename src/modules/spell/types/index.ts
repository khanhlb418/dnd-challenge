export enum School {
  ABJURATION = 'Abjuration',
  CONJURATION = 'Conjuration',
  DIVINATION = 'Divination',
  ENCHANTMENT = 'Enchantment',
  EVOCATION = 'Evocation',
  ILLUSION = 'Illusion',
  NECROMANCY = 'Necromancy',
  TRANSMUTATION = 'Transmutation',
}

export enum ClassIndex {
  FIGHTER = 'fighter',
  BARD = 'bard',
  CLERIC = 'cleric',
  DRUID = 'druid',
  PALADIN = 'paladin',
  RANGER = 'ranger',
  SORCERER = 'sorcerer',
  WARLOCK = 'warlock',
  WIZARD = 'wizard',
}

export interface CharacterClass {
  name: string;
}

export interface RawSpell {
  index: string;
  level: number;
  name: string;
  desc: string[];
  casting_time: string;
  duration: string;
  attack_type?: string;
  range: string;
  damage?: {
    damage_type: {
      name: string;
    };
  };
  school: {
    name: School;
  };
  classes: CharacterClass[];
}

export interface Spell {
  index: string;
  level: number;
  name: string;
  descriptions: string[];
  castingTime: string;
  duration: string;
  attackType?: string;
  range: string;
  damage?: {
    type: string;
  };
  school: School;
  classes: CharacterClass[];
}

export interface Filters {
  classIndex?: ClassIndex | string;
}
