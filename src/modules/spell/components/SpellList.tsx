import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import { memo, useCallback, useMemo, useState } from 'react';

import SpellDetailModal from '@/modules/spell/components/SpellDetailModal';
import SpellItem from '@/modules/spell/components/SpellItem';
import { Spell } from '@/modules/spell/types';

import SpellListHeader from './SpellListHeader';
export interface SpellList {
  list: Spell[];
  isFavoriteSpell: (spell: Spell) => boolean;
  onRemoveFavorite?: (spell: Spell) => void;
  onAddFavorite?: (spell: Spell) => void;
}
const SpellList: React.FC<SpellList> = ({ list, isFavoriteSpell, onAddFavorite, onRemoveFavorite }) => {
  const [selectedSpell, setSelectedSpell] = useState<Spell>();
  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleViewDetails = useCallback((selectedSpell: Spell) => {
    setSelectedSpell(selectedSpell);
    setIsModalOpen(true);
  }, []);

  const closeModal = () => {
    setIsModalOpen(false);
  };

  const handleAddFavorite = useCallback(
    (spell: Spell) => {
      if (onAddFavorite) onAddFavorite(spell);
    },
    [onAddFavorite],
  );

  const handleRemoveFavorite = useCallback(
    (spell: Spell) => {
      if (onRemoveFavorite) onRemoveFavorite(spell);
    },
    [onRemoveFavorite],
  );

  const spellListContent = useMemo(
    () => (
      <>
        {list?.length > 0 ? (
          <Grid container gap={2} flexDirection={{ xs: 'row', lg: 'column' }}>
            {list.map((item) => (
              <Grid
                item
                key={item.index}
                xs={12}
                lg={12}
                sx={{
                  width: '100%',
                  maxWidth: (theme) => ({ sm: `calc(50% - ${theme.spacing(1)})`, lg: '100%' }),
                }}
                flexGrow={1}
              >
                <SpellItem
                  data={item}
                  isFavorite={isFavoriteSpell(item)}
                  onViewDetails={handleViewDetails}
                  onAddFavorite={handleAddFavorite}
                  onRemoveFavorite={handleRemoveFavorite}
                />
              </Grid>
            ))}
          </Grid>
        ) : null}
      </>
    ),
    [list, isFavoriteSpell, handleAddFavorite, handleRemoveFavorite],
  );

  return (
    <>
      <Box>
        <SpellListHeader />
        {spellListContent}
      </Box>
      {selectedSpell && <SpellDetailModal open={isModalOpen} spell={selectedSpell} onClose={closeModal} />}
    </>
  );
};

export default memo(SpellList);
