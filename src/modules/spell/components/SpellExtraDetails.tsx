import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { useTranslation } from 'react-i18next';

import Tag from '@/modules/core/components/Tag';
import { Spell } from '@/modules/spell/types';

interface SpellExtraDetailsProps {
  spell: Spell;
}

const SpellExtraDetails: React.FC<SpellExtraDetailsProps> = ({ spell }) => {
  const { t } = useTranslation();

  return (
    <Grid container flexDirection="column" gap={1}>
      {spell?.descriptions.map((description) => (
        <Grid item key={description}>
          <Typography variant="body2">{description}</Typography>
        </Grid>
      ))}
      <Grid container alignItems="center" gap={1}>
        <Grid item>
          <Typography variant="overline" textTransform="uppercase">
            {t('CLASSES')}:
          </Typography>
        </Grid>
        {spell?.classes?.map(({ name }) => (
          <Grid item key={name}>
            <Tag label={name} />
          </Grid>
        ))}
      </Grid>
    </Grid>
  );
};

export default SpellExtraDetails;
