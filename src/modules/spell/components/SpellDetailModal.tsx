import CloseIcon from '@mui/icons-material/Close';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import Typography from '@mui/material/Typography';
import { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import LabeledValue from '@/modules/core/components/LabeledValue';
import ResponsiveModal from '@/modules/core/components/ResponsiveModal';
import { Spell } from '@/modules/spell/types';
import { getSpellDisplayProperties, getViewSpellDetailsURL } from '@/modules/spell/utils';
import { getSchoolIconURL } from '@/modules/spell/utils';

import SpellExtraDetails from './SpellExtraDetails';

export interface SpellDetailModalProps {
  open?: boolean;
  spell: Spell;
  onClose?: () => void;
}

const SpellDetailModal: React.FC<SpellDetailModalProps> = ({ spell, open = false, onClose }) => {
  const { t } = useTranslation();

  const spellDisplayProperties = useMemo(() => (spell ? getSpellDisplayProperties(spell) : []), [spell]);

  return (
    <ResponsiveModal
      open={open}
      onClose={onClose}
      headerContainerSx={{ borderBottom: 2, borderColor: 'primary.main' }}
      header={
        <>
          <Grid container alignItems="center" gap={1}>
            <Grid item>
              <Tooltip title={spell.school} arrow>
                <Avatar variant="rounded" src={getSchoolIconURL(spell.school)} alt={spell.school} />
              </Tooltip>
            </Grid>
            <Grid item>
              <Typography variant="h5" fontWeight={600}>
                {spell.name}
              </Typography>
            </Grid>
          </Grid>
          {onClose && (
            <IconButton
              aria-label="close"
              onClick={onClose}
              sx={{
                position: 'absolute',
                right: 16,
                top: 16,
                color: 'primary.main',
              }}
            >
              <CloseIcon />
            </IconButton>
          )}
        </>
      }
      content={
        <>
          <Grid container columnGap={4} rowGap={2} sx={{ pt: 2 }}>
            {spellDisplayProperties.map(({ label, value }) => (
              <Grid item key={label} xs={5} lg="auto" flexGrow={1}>
                <LabeledValue label={t(label)} value={value} />
              </Grid>
            ))}
          </Grid>
          <Divider sx={{ borderColor: 'primary.main', borderWidth: 1, marginY: 2 }} />
          <SpellExtraDetails spell={spell} />
        </>
      }
      actions={
        <>
          <Button variant="contained" color="primary" component={Link} to={getViewSpellDetailsURL(spell)}>
            {t('VIEW_DETAILS')}
          </Button>
        </>
      }
    />
  );
};

export default SpellDetailModal;
