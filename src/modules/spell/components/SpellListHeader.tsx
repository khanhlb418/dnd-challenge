import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { memo, useRef } from 'react';
import { useTranslation } from 'react-i18next';

import { usePinned } from '@/modules/core/hooks/usePinned';

const headerColumns = [
  'LEVEL_SHORT_HAND',
  'NAME',
  'CASTING_TIME',
  'DURATION',
  'RANGE',
  'SCHOOL',
  'ATTACK_TYPE',
  'DAMAGE_TYPE',
];

const SpellListHeader = () => {
  const { t } = useTranslation();
  const headerRef = useRef<HTMLDivElement>(null);
  const { isPinned } = usePinned(headerRef);
  return (
    <Box
      ref={headerRef}
      sx={{
        width: '100%',
        height: 50,
        display: { lg: 'flex', xs: 'none' },
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 1,
        position: 'sticky',
        top: 0,
        zIndex: 50,
        backgroundColor: isPinned ? 'primary.main' : 'transparent',
        color: isPinned ? 'common.white' : 'primary.main',
        textTransform: 'uppercase',
        transition: 'all 0.2s',
      }}
    >
      <Box
        sx={{
          width: 40,
        }}
      />
      {headerColumns.map((headerColumn, index) =>
        index === 0 ? (
          <Typography key={headerColumn} sx={{ flex: 0.2, margin: 2 }} variant="h6" fontSize="1rem" fontWeight={700}>
            {t(headerColumn)}
          </Typography>
        ) : (
          <Typography key={headerColumn} sx={{ flex: 1 }} variant="h6" fontSize="1rem" fontWeight={700}>
            {t(headerColumn)}
          </Typography>
        ),
      )}
      <Box
        sx={{
          flex: 0.3,
        }}
      />
    </Box>
  );
};

export default memo(SpellListHeader);
