import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Grid';
import { alpha } from '@mui/material/styles';
import { useTheme } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import { useTranslation } from 'react-i18next';

import LabeledValue from '@/modules/core/components/LabeledValue';
import useToggleFavorite from '@/modules/favorite/hooks/useToggleFavorite';
import { Spell } from '@/modules/spell/types';
import { getSchoolIconURL, getSpellDisplayProperties } from '@/modules/spell/utils';

import SpellExtraDetails from './SpellExtraDetails';

export interface SpellCardProps {
  data: Spell;
  isFavorite: boolean;
  onViewDetails?: (spell: Spell) => void;
  onAddFavorite?: (spell: Spell) => void;
  onRemoveFavorite?: (spell: Spell) => void;
  showFull?: boolean;
}

const SpellCard: React.FC<SpellCardProps> = ({
  data,
  isFavorite,
  onViewDetails,
  onAddFavorite,
  onRemoveFavorite,
  showFull = false,
}) => {
  const { t } = useTranslation();
  const theme = useTheme();
  const spellDisplayProperties = getSpellDisplayProperties(data);
  const { toggleFavoriteButton } = useToggleFavorite({
    spell: data,
    isFavorite,
    onAddFavorite,
    onRemoveFavorite,
  });

  const handleViewDetails = () => {
    if (onViewDetails) onViewDetails(data);
  };

  return (
    <Card
      onClick={handleViewDetails}
      sx={{
        height: '100%',
        background: 'transparent',
        boxShadow: `inset 0 0 ${theme.spacing(1)} ${alpha(theme.palette.primary.main, 0.7)}`,
        border: `1px solid ${alpha(theme.palette.primary.main, 0.7)}`,
        borderRadius: 'unset',
        cursor: 'pointer',
        '&:hover': {
          backgroundColor: (theme) => alpha(theme.palette.primary.main, 0.2),
        },
      }}
      id={data.index}
      data-testid={`spell-item-${data.index}`}
    >
      <CardContent>
        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
            gap: 1,
            marginBottom: 2,
          }}
        >
          <Avatar
            src={getSchoolIconURL(data.school)}
            alt={data.school}
            sx={{ width: showFull ? 48 : 32, height: showFull ? 48 : 32 }}
            variant={showFull ? 'rounded' : 'circular'}
          />
          <Typography variant={showFull ? 'h5' : 'h6'} fontWeight={600} data-testid={`spell-item-name-${data.index}`}>
            {data.name}
          </Typography>
          <Typography variant="body1">{`${t('LEVEL_SHORT_HAND')} ${data.level}`}</Typography>
          <Box sx={{ flex: 1, display: 'flex', justifyContent: 'end' }}>{toggleFavoriteButton}</Box>
        </Box>

        <Grid container alignItems="base-line" justifyContent="flex-start" gap={1}>
          {spellDisplayProperties.map(({ label, value }) => (
            <Grid
              item
              sx={{
                width: '100%',
                maxWidth: (theme) => ({
                  xs: `calc(50% - ${theme.spacing(1)})`,
                }),
              }}
              lg={3}
              key={label}
            >
              <LabeledValue label={t(label)} value={value} />
            </Grid>
          ))}
        </Grid>

        {showFull && (
          <>
            <Divider sx={{ borderColor: 'primary.main', borderWidth: 0.5, marginY: 2 }} />
            <SpellExtraDetails spell={data} />
          </>
        )}
      </CardContent>
    </Card>
  );
};

export default SpellCard;
