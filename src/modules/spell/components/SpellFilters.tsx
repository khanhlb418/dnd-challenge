import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { ClassIndex, Filters } from '@/modules/spell/types';
import { getClassIconURL } from '@/modules/spell/utils';

import { availableClasses } from '../services/spell';

interface SpellFiltersProps {
  selectedClass?: ClassIndex | string;
  onApplyFilters?: (filters: Filters) => void;
}

const SpellFilters: React.FC<SpellFiltersProps> = ({ selectedClass, onApplyFilters }) => {
  const { t } = useTranslation();

  const handleChangeClass = (classIndex: ClassIndex | string) => () => {
    if (!onApplyFilters) return;

    onApplyFilters({
      classIndex,
    });
  };

  return (
    <Box
      sx={{
        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',
        margin: { xs: 2, md: 4 },
        gap: { xs: 2, md: 4 },
      }}
    >
      {(availableClasses as ClassIndex[]).map((availableClass) => (
        <Box
          key={availableClass}
          onClick={handleChangeClass(availableClass)}
          sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            textTransform: 'uppercase',
            cursor: 'pointer',
            opacity: selectedClass === availableClass ? 1 : 0.3,
            '&:hover': {
              opacity: 1,
            },
          }}
        >
          <Avatar
            src={getClassIconURL(availableClass)}
            alt={availableClass}
            sx={{
              height: { xs: 40, md: 64 },
              width: { xs: 40, md: 64 },
            }}
          />
          <Typography variant="body2" fontWeight={600} marginTop={0.5}>
            {t(availableClass.toUpperCase())}
          </Typography>
        </Box>
      ))}
    </Box>
  );
};

export default SpellFilters;
