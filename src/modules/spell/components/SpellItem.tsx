import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import { alpha } from '@mui/material/styles';
import { useTheme } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import useMediaQuery from '@mui/material/useMediaQuery';
import { memo } from 'react';

import useToggleFavorite from '@/modules/favorite/hooks/useToggleFavorite';
import { Spell } from '@/modules/spell/types';
import { getSchoolIconURL } from '@/modules/spell/utils';

import SpellCard from './SpellCard';

export interface SpellItemProps {
  data: Spell;
  isFavorite?: boolean;
  onViewDetails: (spell: Spell) => void;
  onAddFavorite?: (spell: Spell) => void;
  onRemoveFavorite?: (spell: Spell) => void;
}

const SpellItem: React.FC<SpellItemProps> = ({
  data,
  isFavorite = false,
  onViewDetails,
  onAddFavorite,
  onRemoveFavorite,
}) => {
  const theme = useTheme();
  const isLGUp = useMediaQuery(theme.breakpoints.up('lg'));
  const { toggleFavoriteButton } = useToggleFavorite({
    spell: data,
    isFavorite,
    onAddFavorite,
    onRemoveFavorite,
  });

  const handleViewDetails = () => {
    onViewDetails(data);
  };

  if (isLGUp) {
    return (
      <Box
        onClick={handleViewDetails}
        sx={{
          width: '100%',
          display: 'flex',
          height: 64,
          justifyContent: 'space-between',
          alignItems: 'center',
          padding: 1,
          cursor: 'pointer',
          boxShadow: `inset 0 0 ${theme.spacing(0.5)} ${theme.palette.primary.main}`,
          border: `1px solid ${theme.palette.primary.main}`,
          '&:hover': {
            backgroundColor: (theme) => alpha(theme.palette.primary.main, 0.2),
          },
        }}
        id={data.index}
        data-testid={`spell-item-${data.index}`}
      >
        <Avatar src={getSchoolIconURL(data.school)} alt={data.school} />
        <Typography sx={{ flex: 0.2, margin: 2 }}>{data.level}</Typography>
        <Typography sx={{ flex: 1 }} data-testid={`spell-item-name-${data.index}`}>
          {data.name}
        </Typography>
        <Typography sx={{ flex: 1 }}>{data.castingTime}</Typography>
        <Typography sx={{ flex: 1 }}>{data.duration}</Typography>
        <Typography sx={{ flex: 1 }}>{data.range}</Typography>
        <Typography sx={{ flex: 1 }}>{data.school}</Typography>
        <Typography sx={{ flex: 1 }}>{data.attackType}</Typography>
        <Typography sx={{ flex: 1 }}>{data.damage?.type}</Typography>
        <Box sx={{ flex: 0.3 }}>{toggleFavoriteButton}</Box>
      </Box>
    );
  }

  return (
    <SpellCard
      data={data}
      isFavorite={isFavorite}
      onViewDetails={handleViewDetails}
      onAddFavorite={onAddFavorite}
      onRemoveFavorite={onRemoveFavorite}
    />
  );
};
export default memo(SpellItem);
