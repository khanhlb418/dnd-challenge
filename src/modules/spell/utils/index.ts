import { truthy } from '@/modules/core/types';
import { ClassIndex, School, Spell } from '@/modules/spell/types';

export const getSchoolIconURL = (school?: School) => {
  return `/school-icons/${school?.toLowerCase()}.png`;
};

export const getViewSpellDetailsURL = (spell?: Spell) => {
  return `/spells/${spell?.index}`;
};

export const getClassIconURL = (className: ClassIndex) => {
  return `/class-icons/${className.toLowerCase()}.jpg`;
};

export const getSpellDisplayProperties = (spell: Spell) => {
  return [
    {
      label: 'CASTING_TIME',
      value: spell.castingTime,
    },
    {
      label: 'DURATION',
      value: spell.duration,
    },
    {
      label: 'RANGE',
      value: spell.range,
    },
    {
      label: 'SCHOOL',
      value: spell.school,
    },
    spell.attackType && {
      label: 'ATTACK',
      value: spell.attackType,
    },
    spell.damage && {
      label: 'DAMAGE',
      value: spell.damage.type,
    },
  ].filter(truthy);
};
