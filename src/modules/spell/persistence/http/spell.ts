import { gql, request } from 'graphql-request';

import { DEFAULT_LIMIT, DEFAULT_OFFSET, GRAPHQL_API_ORIGIN_URL, REST_API_ORIGIN_URL } from '@/config';
import { joinOriginAndURL } from '@/modules/core/utils/url';
import { ClassIndex, RawSpell, Spell } from '@/modules/spell/types';

import { ALL_CLASSES_KEY } from '../../services/spell';

export const SPELL_CACHE_KEYS = {
  FETCH_SPELLS: 'fetch-spells',
  FETCH_SPELL: 'fetch-spell',
};

export interface FetchSpellsResponse {
  spells: Spell[];
  count: number;
}

export interface FetchSpellsOptions {
  limit?: number;
  offset?: number;
  classIndex?: ClassIndex | string;
}

const fetchSpellsQuery = gql`
  query fetchSpellsQuery($limit: Int!, $offset: Int!, $classIndex: StringFilter) {
    spells(limit: $limit, skip: $offset, class: $classIndex) {
      index
      level
      name
      desc
      casting_time
      duration
      attack_type
      range
      damage {
        damage_type {
          name
        }
      }
      school {
        name
        index
      }
      classes {
        name
      }
    }
  }
`;

const fetchSpellQuery = gql`
  query fetchSpellQuery($index: String!) {
    spell(index: $index) {
      index
      level
      name
      desc
      casting_time
      duration
      attack_type
      range
      damage {
        damage_type {
          name
        }
      }
      school {
        name
        index
      }
      classes {
        name
      }
    }
  }
`;

export const fetchSpells = async ({
  limit = DEFAULT_LIMIT,
  offset = DEFAULT_OFFSET,
  classIndex = ALL_CLASSES_KEY,
}: FetchSpellsOptions): Promise<FetchSpellsResponse> => {
  const [{ spells: rawSpells }, count] = await Promise.all([
    request(GRAPHQL_API_ORIGIN_URL, fetchSpellsQuery, {
      limit,
      offset,
      classIndex: classIndex === ALL_CLASSES_KEY ? undefined : classIndex,
    }),
    fetchSpellCount(classIndex),
  ]);

  return { spells: rawSpells.map(normalizeSpell), count };
};

export const fetchSpell = async (index: string): Promise<Spell> => {
  const response = await request(GRAPHQL_API_ORIGIN_URL, fetchSpellQuery, { index });

  return normalizeSpell(response.spell);
};

export const fetchSpellCount = async (classIndex?: ClassIndex | string): Promise<number> => {
  const fetchURL = joinOriginAndURL(
    REST_API_ORIGIN_URL,
    classIndex === ALL_CLASSES_KEY ? '/api/spells' : `/api/classes/${classIndex}/spells`,
  );

  const response = await fetch(fetchURL);
  const { count } = await response.json();

  return count;
};

const normalizeSpell = (rawSpell: RawSpell): Spell => {
  return {
    index: rawSpell.index,
    level: rawSpell.level,
    name: rawSpell.name,
    descriptions: rawSpell.desc,
    castingTime: rawSpell.casting_time,
    duration: rawSpell.duration,
    attackType: rawSpell.attack_type,
    range: rawSpell.range,
    ...(rawSpell.damage?.damage_type?.name
      ? {
          damage: {
            type: rawSpell.damage.damage_type.name,
          },
        }
      : {}),
    school: rawSpell.school.name,
    classes: rawSpell.classes,
  };
};
