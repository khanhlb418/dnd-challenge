import { ClassIndex } from '@/modules/spell/types';

export const ALL_CLASSES_KEY = 'all_classes';

export const availableClasses: (ClassIndex | string)[] = [ALL_CLASSES_KEY, ...Object.values(ClassIndex)];
