import Box from '@mui/material/Box';
import Pagination from '@mui/material/Pagination';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useQuery } from '@tanstack/react-query';
import { createSearchParams, useNavigate, useSearchParams } from 'react-router-dom';

import { DEFAULT_LIMIT } from '@/config';
import EmptyPlaceholder from '@/modules/core/components/EmptyPlaceholder';
import ErrorPlaceholder from '@/modules/core/components/ErrorPlaceholder';
import LoadingPlaceholder, { FullscreenLoadingPlaceholder } from '@/modules/core/components/LoadingPlaceholder';
import { useFavoriteSpells } from '@/modules/favorite/hooks/useFavoriteSpells';
import { usePrevious } from '@/modules/favorite/hooks/usePrevious';
import SpellFilters from '@/modules/spell/components/SpellFilters';
import SpellList from '@/modules/spell/components/SpellList';
import { fetchSpells, SPELL_CACHE_KEYS } from '@/modules/spell/persistence/http/spell';
import { ALL_CLASSES_KEY } from '@/modules/spell/services/spell';
import { ClassIndex, Filters } from '@/modules/spell/types';

const ITEMS_PER_PAGE = DEFAULT_LIMIT;

const createSearchForNavigate = ({ page, classIndex }: { page?: number; classIndex?: ClassIndex | string }) => {
  return `?${createSearchParams({
    ...(page ? { page: String(page) } : {}),
    ...(classIndex ? { classIndex } : {}),
  })}`;
};

const SpellListPage = () => {
  const theme = useTheme();
  const isSMUp = useMediaQuery(theme.breakpoints.up('sm'));
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();
  const page = Number(searchParams.get('page') || 1);
  const classIndex = searchParams.get('classIndex') || ALL_CLASSES_KEY;
  const offset = (page - 1) * ITEMS_PER_PAGE;
  const {
    data: currentResponse,
    isLoading,
    error,
  } = useQuery([SPELL_CACHE_KEYS.FETCH_SPELLS, page, classIndex], () =>
    fetchSpells({ limit: DEFAULT_LIMIT, offset, classIndex }),
  );
  const previousResponse = usePrevious(currentResponse, true);
  const fetchSpellsResponse = currentResponse || previousResponse;
  const { isFavoriteSpell, removeFavoriteSpell, addFavoriteSpell } = useFavoriteSpells();

  const handleChangePagination = (event: React.ChangeEvent<unknown>, value: number) => {
    navigate({
      pathname: '/',
      search: createSearchForNavigate({ page: value, classIndex }),
    });
  };

  const handleApplyFilters = (filters: Filters) => {
    navigate({
      pathname: '/',
      search: createSearchForNavigate({
        page: 1,
        classIndex: filters.classIndex === ALL_CLASSES_KEY ? '' : filters.classIndex,
      }),
    });
  };

  let content: React.ReactNode = <FullscreenLoadingPlaceholder />;

  if (fetchSpellsResponse) {
    content =
      fetchSpellsResponse.spells.length === 0 ? (
        <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexGrow: 1, mt: 8 }}>
          <EmptyPlaceholder />
        </Box>
      ) : (
        <>
          {isLoading && <FullscreenLoadingPlaceholder />}
          <SpellList
            list={fetchSpellsResponse.spells}
            isFavoriteSpell={isFavoriteSpell}
            onAddFavorite={addFavoriteSpell}
            onRemoveFavorite={removeFavoriteSpell}
          />
          <Pagination
            count={Math.ceil(fetchSpellsResponse.count / ITEMS_PER_PAGE)}
            page={page}
            size={isSMUp ? 'medium' : 'small'}
            onChange={handleChangePagination}
            sx={{ mt: 2, display: 'flex', justifyContent: 'center' }}
          />
        </>
      );
  } else if (error) {
    content = (
      <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexGrow: 1, mt: 8 }}>
        <ErrorPlaceholder />
      </Box>
    );
  }

  return (
    <Box sx={{ padding: 1.5 }}>
      <SpellFilters selectedClass={classIndex} onApplyFilters={handleApplyFilters} />
      {content}
    </Box>
  );
};

export default SpellListPage;
