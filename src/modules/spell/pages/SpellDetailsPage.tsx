import Box from '@mui/material/Box';
import { useQuery } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';

import ErrorPlaceholder from '@/modules/core/components/ErrorPlaceholder';
import LoadingPlaceholder from '@/modules/core/components/LoadingPlaceholder';
import { useFavoriteSpells } from '@/modules/favorite/hooks/useFavoriteSpells';
import SpellCard from '@/modules/spell/components/SpellCard';
import { fetchSpell, SPELL_CACHE_KEYS } from '@/modules/spell/persistence/http/spell';

const SpellDetailsPage = () => {
  const params = useParams();
  const index = String(params.index);
  const { data, isLoading, error } = useQuery([SPELL_CACHE_KEYS, index], () => fetchSpell(index));
  const { isFavoriteSpell, addFavoriteSpell, removeFavoriteSpell } = useFavoriteSpells();

  if (isLoading) {
    return (
      <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexGrow: 1, mt: 4 }}>
        <LoadingPlaceholder />
      </Box>
    );
  }

  if (error || !data) {
    return (
      <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexGrow: 1, mt: 4 }}>
        <ErrorPlaceholder />
      </Box>
    );
  }

  return (
    <Box sx={{ padding: 1.5 }}>
      <SpellCard
        data={data}
        isFavorite={isFavoriteSpell(data)}
        onAddFavorite={addFavoriteSpell}
        onRemoveFavorite={removeFavoriteSpell}
        showFull
      />
    </Box>
  );
};

export default SpellDetailsPage;
