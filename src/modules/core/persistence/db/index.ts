import localForage from 'localforage';

export const saveToDB = (key: string, value: string) => {
  return localForage.setItem(key, value);
};

export const getFromDB = async (key: string) => {
  return (await localForage.getItem(key)) as string;
};

export const removeFromDB = (key: string) => {
  localForage.removeItem(key);
};
