import { createBrowserRouter } from 'react-router-dom';

import Layout from '@/modules/core/components/Layout';
import FavoritePage from '@/modules/favorite/pages/FavoritePage';
import SpellDetailsPage from '@/modules/spell/pages/SpellDetailsPage';
import SpellListPage from '@/modules/spell/pages/SpellListPage';

export const router = createBrowserRouter([
  {
    element: <Layout />,
    children: [
      {
        path: '/',
        element: <SpellListPage />,
      },
      {
        path: '/favorite',
        element: <FavoritePage />,
      },
      {
        path: '/spells/:index',
        element: <SpellDetailsPage />,
      },
    ],
  },
]);
