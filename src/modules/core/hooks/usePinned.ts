import React, { useEffect, useState } from 'react';

export const usePinned = (elementRef: React.RefObject<HTMLElement>) => {
  const [isPinned, setIsPinned] = useState(false);

  useEffect(() => {
    if (!elementRef.current) return;

    const observer = new IntersectionObserver(
      ([entry]) => {
        if (entry.intersectionRatio < 1) return setIsPinned(true);

        setIsPinned(false);
      },
      { threshold: [1], rootMargin: '-1px' },
    );

    observer.observe(elementRef.current);

    return () => {
      observer.disconnect();
    };
  }, [elementRef]);

  return { isPinned };
};
