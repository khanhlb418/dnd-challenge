import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { useTranslation } from 'react-i18next';

import Wizard from '@/modules/core/icons/Wizard';

const EmptyPlaceholder = () => {
  const { t } = useTranslation();

  return (
    <Box
      sx={{
        position: 'relative',
        display: 'flex',
        flexDirection: 'column',
        width: 'fit-content',
        alignItems: 'center',
      }}
    >
      <Wizard sx={{ width: 240, height: 240, opacity: 0.9 }} color="primary" />
      <Typography
        variant="h5"
        color="primary.main"
        fontWeight={500}
        fontFamily="'Henny Penny', monospace"
        textAlign="center"
      >
        {t('NO_DATA_PLACEHOLDER')}
      </Typography>
    </Box>
  );
};

export default EmptyPlaceholder;
