import { PaperProps } from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Drawer from '@mui/material/Drawer';
import { SxProps, Theme, useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';

export interface ResponsiveModalProps {
  open?: boolean;
  onClose?: () => void;
  headerContainerSx?: SxProps<Theme>;
  header?: React.ReactNode;
  content?: React.ReactNode;
  actions?: React.ReactNode;
}

const ResponsiveModal: React.FC<ResponsiveModalProps> = ({
  open = false,
  headerContainerSx,
  header,
  content,
  actions,
  onClose,
}) => {
  const theme = useTheme();
  const isSMUp = useMediaQuery(theme.breakpoints.up('sm'));

  const modalContent = (
    <>
      {header && <DialogTitle sx={headerContainerSx}>{header}</DialogTitle>}
      {content && <DialogContent>{content}</DialogContent>}
      {actions && (
        <DialogActions
          sx={{ justifyContent: 'flex-start', paddingTop: 0, paddingRight: 2.5, paddingBottom: 2.5, paddingLeft: 2.5 }}
        >
          {actions}
        </DialogActions>
      )}
    </>
  );

  const basePaperProps: PaperProps<'div'> = {
    sx: {
      background: 'url("/vintage-paper-background-2.jpeg") no-repeat center center',
      backgroundColor: 'common.white',
      backgroundSize: 'cover',
    },
  };

  if (isSMUp) {
    return (
      <Dialog open={open} onClose={onClose} PaperProps={basePaperProps} disableScrollLock>
        {modalContent}
      </Dialog>
    );
  }

  return (
    <Drawer
      anchor="bottom"
      open={open}
      onClose={onClose}
      PaperProps={{ sx: { ...basePaperProps.sx, maxHeight: '85vh', borderTopLeftRadius: 8, borderTopRightRadius: 8 } }}
    >
      {modalContent}
    </Drawer>
  );
};

export default ResponsiveModal;
