import Box from '@mui/material/Box';
import CircularProgress from '@mui/material/CircularProgress';
import Typography from '@mui/material/Typography';
import { useTranslation } from 'react-i18next';

import Logo from '@/modules/core/icons/Logo';

interface LoadingPlaceholderProps {
  hideText?: boolean;
  opacity?: number;
}

const LoadingPlaceholder: React.FC<LoadingPlaceholderProps> = ({ hideText = false, opacity }) => {
  const { t } = useTranslation();

  return (
    <Box
      sx={{
        position: 'relative',
        display: 'flex',
        flexDirection: 'column',
        width: 'fit-content',
        alignItems: 'center',
        zIndex: 101,
      }}
    >
      <Logo sx={{ width: 240, height: 240, opacity: opacity ?? 0.9 }} color="primary" />
      <CircularProgress
        sx={{ left: 30, top: 30, position: 'absolute', opacity: opacity ?? 0.9 }}
        color="primary"
        size={180}
        thickness={1}
        disableShrink
      />
      {!hideText && (
        <Typography
          variant="h5"
          color="primary.main"
          fontWeight={500}
          fontFamily="'Henny Penny', monospace"
          textAlign="center"
        >
          {t('LOADING')}
        </Typography>
      )}
    </Box>
  );
};

export const FullscreenLoadingPlaceholder = () => {
  return (
    <Box
      sx={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'fixed',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        zIndex: 100,
      }}
    >
      <Box
        sx={{
          position: 'fixed',
          top: 0,
          right: 0,
          bottom: 0,
          left: 0,
          zIndex: 99,
          backgroundColor: 'common.black',
          opacity: 0.3,
        }}
      />
      <LoadingPlaceholder hideText opacity={1} />
    </Box>
  );
};

export default LoadingPlaceholder;
