import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { useTranslation } from 'react-i18next';

import Fighter from '@/modules/core/icons/Fighter';

const ErrorPlaceholder = () => {
  const { t } = useTranslation();

  return (
    <Box
      sx={{
        position: 'relative',
        display: 'flex',
        flexDirection: 'column',
        width: 'fit-content',
        alignItems: 'center',
      }}
    >
      <Fighter sx={{ width: 240, height: 240, opacity: 0.9 }} color="primary" />
      <Typography
        variant="h5"
        color="primary.main"
        fontWeight={500}
        fontFamily="'Henny Penny', monospace"
        textAlign="center"
      >
        {t('ERROR_PLACEHOLDER')}
      </Typography>
    </Box>
  );
};

export default ErrorPlaceholder;
