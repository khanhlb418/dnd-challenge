import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import DragonEye from '@/modules/core/icons/DragonEye';
import Logo from '@/modules/core/icons/Logo';

const CustomAppBar = () => {
  const { t } = useTranslation();

  return (
    <AppBar position="static">
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Link to="/" style={{ textDecoration: 'none', color: 'white' }}>
            <Grid container spacing={1} alignItems="center">
              <Grid item>
                <Logo sx={{ display: 'flex', fontSize: 56 }} />
              </Grid>
              <Grid item>
                <Typography
                  variant="h6"
                  noWrap
                  sx={{
                    mr: 2,
                    display: 'flex',
                    fontFamily: "'Henny Penny', monospace",
                    fontWeight: 700,
                    letterSpacing: '.3rem',
                    color: 'inherit',
                    textDecoration: 'none',
                  }}
                >
                  D&D SPELLS
                </Typography>
              </Grid>
            </Grid>
          </Link>

          <Box sx={{ flexGrow: 1 }} />

          <Box sx={{ flexGrow: 0 }}>
            <Button sx={{ color: 'common.white', display: { xs: 'none', sm: 'flex' } }} component={Link} to="/favorite">
              <DragonEye fontSize="large" />
              <Typography sx={{ ml: 1, fontFamily: "'Henny Penny', monospace" }} fontWeight={700}>
                {t('FAVORITE_SPELLS')}
              </Typography>
            </Button>
            <IconButton sx={{ display: { sm: 'none' }, color: 'common.white' }} component={Link} to="/favorite">
              <DragonEye fontSize="large" />
            </IconButton>
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
};
export default CustomAppBar;
