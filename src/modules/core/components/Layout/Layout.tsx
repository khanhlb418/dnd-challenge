import Box from '@mui/material/Box';
import { Outlet } from 'react-router-dom';

import CustomAppBar from './AppBar';

const Layout = () => {
  return (
    <Box sx={{ display: 'flex', flexDirection: 'column', height: '100%', flexGrow: 1 }}>
      <CustomAppBar />
      <Box
        sx={{
          background: 'url("/vintage-paper-background-1.jpeg") no-repeat center center fixed',
          backgroundSize: 'cover',
          flexGrow: 1,
        }}
      >
        <Outlet />
      </Box>
    </Box>
  );
};

export default Layout;
