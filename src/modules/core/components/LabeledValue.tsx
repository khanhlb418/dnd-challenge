import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

export interface ReadonlyTextField {
  label?: string;
  value?: string | number;
}

const LabeledValue: React.FC<ReadonlyTextField> = ({ label, value }) => {
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <Typography variant="body2" fontWeight={700} textTransform="uppercase" color="grey.800">
        {label}
      </Typography>
      <Typography variant="body1">{value}</Typography>
    </Box>
  );
};

export default LabeledValue;
