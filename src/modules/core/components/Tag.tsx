import Chip from '@mui/material/Chip';
import { styled } from '@mui/material/styles';

const CustomChip = styled(Chip)(({ theme }) => ({
  background: 'transparent',
  boxShadow: `inset 0 0 ${theme.spacing(0.5)} ${theme.palette.grey[400]}`,
  border: `1px solid ${theme.palette.grey[400]}`,
  borderRadius: 0,
  textTransform: 'uppercase',
  fontWeight: 700,
  fontSize: '0.625rem',
}));

interface TagProps {
  label: string;
}

const Tag: React.FC<TagProps> = ({ label }) => {
  return <CustomChip label={label} size="small" />;
};

export default Tag;
