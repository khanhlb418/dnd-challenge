export const joinOriginAndURL = (base: string, url: string) => {
  return new URL(url, base).href;
};
