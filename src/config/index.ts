export const GRAPHQL_API_ORIGIN_URL = 'https://www.dnd5eapi.co/graphql';

export const REST_API_ORIGIN_URL = 'https://www.dnd5eapi.co/api';

export const DEFAULT_OFFSET = 0;

export const DEFAULT_LIMIT = 10;

export const FAVORITE_SPELLS_DB_KEY = 'favorite_spells';
