import { pathsToModuleNameMapper } from 'ts-jest/utils';

module.exports = {
  moduleNameMapper: {       
    '@/(.*)': '<rootDir>/src/$1'
  },
  testEnvironment: 'jsdom',
}

