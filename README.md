## DnD Frontend Challenge
A Dungeons & Dragons spell listing app. The APIs are available at www.dnd5eapi.co.

### Adopted Technologies
- [ReactJS](https://reactjs.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [React Query](https://tanstack.com/query/v4/docs/overview)
- [React Router](https://reactrouter.com/en/main)
- [MUI (CSS in JS)](https://mui.com/)
- [Jest (testing framework)](https://jestjs.io/)

### Development Instructions

Install all dependencies
```bash
yarn
```
Run the development server
```bash
yarn dev
```

Open [http://localhost:5173](http://localhost:5173) with your browser and start coding.

### Testing
Run unit tests on development
```bash
yarn test
```

E2E tests can be referred on [this repository](https://gitlab.com/khanhlb418/dnd-challenge-e2e-tests).

### Live Demo
The live demo is available at https://dnd-spellz.netlify.app/