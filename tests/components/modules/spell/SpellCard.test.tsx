import { render, screen } from '@testing-library/react';
import React from 'react';

import SpellCard from '../../../../src/modules/spell/components/SpellCard';
import { School } from '../../../../src/modules/spell/types';

jest.mock('react-i18next', () => ({
  // this mock makes sure any components using the translate hook can use it without a warning being shown
  useTranslation: () => {
    return {
      t: (str: string) => str,
      i18n: {
        changeLanguage: () =>
          new Promise(() => {
            return null;
          }),
      },
    };
  },
}));

describe('SpellCard component', () => {
  const spell = {
    index: 'acid-arrow',
    name: 'Acid arrow',
    level: 0,
    descriptions: ['A powerful skill'],
    castingTime: '1 action',
    range: '30 feet',
    school: School.CONJURATION,
    classes: [{ name: 'ranger' }],
  };

  test('should render a spell card with selected fields', async () => {
    render(<SpellCard data={spell} isFavorite={true} />);

    const spellNameValue = screen.getByText(spell.name);
    const castingTimeValue = screen.getByText(spell.castingTime);
    const schoolValue = screen.getByText(spell.school);
    const rangeValue = screen.getByText(spell.range);
    const schoolImage = screen.queryByAltText(spell.school);

    expect(spellNameValue).toBeTruthy();
    expect(castingTimeValue).toBeTruthy();
    expect(schoolValue).toBeTruthy();
    expect(rangeValue).toBeTruthy();
    expect(schoolImage).toBeTruthy();
  });

  test('should render a spell card without descriptions and classes on partial display', async () => {
    render(<SpellCard data={spell} isFavorite={true} showFull={false} />);

    const spellDescriptionsValue = screen.queryByText(spell.descriptions[0]);
    const spellClassesValue = screen.queryByText(spell.classes[0].name);

    expect(spellDescriptionsValue).toBeFalsy();
    expect(spellClassesValue).toBeFalsy();
  });

  test('should render a spell card with descriptions and classes on full display', async () => {
    render(<SpellCard data={spell} isFavorite={true} showFull={true} />);

    const spellDescriptionsValue = screen.getByText(spell.descriptions[0]);
    const spellClassesValue = screen.getByText(spell.classes[0].name);

    expect(spellDescriptionsValue).toBeTruthy();
    expect(spellClassesValue).toBeTruthy();
  });
});
