import * as favoritePersistenceDB from '../../../../../src/modules/favorite/persistence/db/favorite';
import { FavoriteSpellService } from '../../../../../src/modules/favorite/services/favoriteSpell';
import { School, Spell } from '../../../../../src/modules/spell/types';

describe('FavoriteSpellService', () => {
  let initialFavoriteSpellList: Spell[];

  const spell1: Spell = {
    index: 'acid-arrow',
    name: 'Acid Arrow',
    duration: '24 hours',
    level: 0,
    descriptions: ['A powerful skill'],
    castingTime: '1 action',
    range: 'RANGE',
    school: School.CONJURATION,
    classes: [{ name: 'ranger' }, { name: 'druid' }],
  };

  const spell2: Spell = {
    index: 'arcane-eye',
    name: 'Arcane Eye',
    duration: '10 hours',
    level: 1,
    descriptions: ['Create an invisible eye'],
    castingTime: '1 action',
    range: 'RANGE',
    school: School.DIVINATION,
    classes: [{ name: 'cleric' }],
  };

  afterEach(() => {
    jest.restoreAllMocks();
  });

  describe('addFavoriteSpell method', () => {
    beforeEach(() => {
      initialFavoriteSpellList = [spell2];
    });

    test('should append new favorite spell then save all of the spells to DB', async () => {
      const getFavoriteSpellSpy = jest
        .spyOn(FavoriteSpellService, 'getFavoriteSpells')
        .mockResolvedValue(initialFavoriteSpellList);
      const saveFavoriteSpellsToDBSpy = jest
        .spyOn(favoritePersistenceDB, 'saveFavoriteSpellsToDB')
        .mockImplementation(() => null);
      const expectedSpellListToBeSaved = [...initialFavoriteSpellList, spell1];

      await FavoriteSpellService.addFavoriteSpell(spell1);

      expect(getFavoriteSpellSpy).toBeCalledTimes(1);
      expect(saveFavoriteSpellsToDBSpy).toBeCalledTimes(1);
      expect(saveFavoriteSpellsToDBSpy).toBeCalledWith(expectedSpellListToBeSaved);
    });

    test('should throw error if failed to get favorite spells', async () => {
      const error = new Error('Failed to get favorite spells');
      const getFavoriteSpellSpy = jest.spyOn(FavoriteSpellService, 'getFavoriteSpells').mockRejectedValue(error);
      const saveFavoriteSpellsToDBSpy = jest
        .spyOn(favoritePersistenceDB, 'saveFavoriteSpellsToDB')
        .mockImplementation(() => null);

      expect.assertions(3);
      try {
        await FavoriteSpellService.addFavoriteSpell(spell1);
      } catch (err) {
        expect(getFavoriteSpellSpy).toBeCalledTimes(1);
        expect(saveFavoriteSpellsToDBSpy).toBeCalledTimes(0);
        expect((err as Error).message).toBe(error.message);
      }
    });
  });

  describe('removeFavoriteSpell method', () => {
    beforeEach(() => {
      initialFavoriteSpellList = [spell1, spell2];
    });

    test('should remove a specific spell then save favorite spells to DB', async () => {
      const getFavoriteSpellSpy = jest
        .spyOn(FavoriteSpellService, 'getFavoriteSpells')
        .mockResolvedValue(initialFavoriteSpellList);
      const saveFavoriteSpellsToDBSpy = jest
        .spyOn(favoritePersistenceDB, 'saveFavoriteSpellsToDB')
        .mockImplementation(() => null);
      const spellsToBeSaved = initialFavoriteSpellList.filter((spell) => spell.index !== spell1.index);

      await FavoriteSpellService.removeFavoriteSpell(spell1);

      expect(getFavoriteSpellSpy).toBeCalledTimes(1);
      expect(saveFavoriteSpellsToDBSpy).toBeCalledTimes(1);
      expect(saveFavoriteSpellsToDBSpy).toBeCalledWith(spellsToBeSaved);
    });

    test('should throw error if failed to get favorite spells', async () => {
      const error = new Error('Failed to get favorite spells');
      const getFavoriteSpellSpy = jest.spyOn(FavoriteSpellService, 'getFavoriteSpells').mockRejectedValue(error);
      const saveFavoriteSpellsToDBSpy = jest
        .spyOn(favoritePersistenceDB, 'saveFavoriteSpellsToDB')
        .mockImplementation(() => null);

      expect.assertions(3);
      try {
        await FavoriteSpellService.removeFavoriteSpell(spell1);
      } catch (err) {
        expect(getFavoriteSpellSpy).toBeCalledTimes(1);
        expect(saveFavoriteSpellsToDBSpy).toBeCalledTimes(0);
        expect((err as Error).message).toBe(error.message);
      }
    });
  });

  describe('getFavoriteSpells method', () => {
    beforeEach(() => {
      initialFavoriteSpellList = [spell1, spell2];
    });

    test('should resolve a spell list', async () => {
      const getFavoriteSpellsFromDBSpy = jest
        .spyOn(favoritePersistenceDB, 'getFavoriteSpellsFromDB')
        .mockResolvedValue([spell1, spell2]);

      const result = await FavoriteSpellService.getFavoriteSpells();

      expect(result).toEqual(expect.arrayContaining(initialFavoriteSpellList));
      expect(getFavoriteSpellsFromDBSpy).toBeCalledTimes(1);
    });

    test('should throw error if failed to get favorite spells', async () => {
      let result;
      const error = new Error('Failed to get favorite spells');

      const getFavoriteSpellsFromDBSpy = jest
        .spyOn(favoritePersistenceDB, 'getFavoriteSpellsFromDB')
        .mockRejectedValue(error);

      expect.assertions(3);
      try {
        result = await FavoriteSpellService.getFavoriteSpells();
      } catch (err) {
        expect(result).toBeUndefined();
        expect(getFavoriteSpellsFromDBSpy).toBeCalledTimes(1);
        expect((err as Error).message).toBe(error.message);
      }
    });
  });
});
